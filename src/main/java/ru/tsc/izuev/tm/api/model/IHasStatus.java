package ru.tsc.izuev.tm.api.model;

import ru.tsc.izuev.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
