package ru.tsc.izuev.tm.api.model;

public interface ICommand {

    void execute();

    String getArgument();

    String getDescription();

    String getName();

}
