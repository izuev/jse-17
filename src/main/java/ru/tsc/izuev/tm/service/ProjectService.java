package ru.tsc.izuev.tm.service;

import ru.tsc.izuev.tm.api.repository.IProjectRepository;
import ru.tsc.izuev.tm.api.service.IProjectService;
import ru.tsc.izuev.tm.enumerated.Sort;
import ru.tsc.izuev.tm.enumerated.Status;
import ru.tsc.izuev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.izuev.tm.exception.field.*;
import ru.tsc.izuev.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public final class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Sort sort) {
        if (sort == null) return findAll();
        final Comparator<Project> comparator = sort.getComparator();
        if (comparator == null) return findAll();
        return findAll(comparator);
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null) return findAll();
        return projectRepository.findAll(comparator);
    }

    @Override
    public Project findById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.findById(id);
    }

    @Override
    public Project findByIndex(final Integer index) {
        if (index == null || index < 0 || index >= projectRepository.size()) throw new IndexIncorrectException();
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @Override
    public Project add(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        return projectRepository.add(project);
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0 || index >= projectRepository.size()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        final Project project = findById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0 || index >= projectRepository.size()) throw new IndexIncorrectException();
        if (status == null) throw new StatusIncorrectException();
        final Project project = findByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project remove(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        return projectRepository.remove(project);
    }

    @Override
    public Project removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeByIndex(final Integer index) {
        if (index == null || index < 0 || index >= projectRepository.size()) throw new IndexIncorrectException();
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Integer size() {
        return projectRepository.size();
    }

    @Override
    public boolean notExistsById(final String id) {
        if (id == null || id.isEmpty()) return false;
        return projectRepository.notExistsById(id);
    }

}
