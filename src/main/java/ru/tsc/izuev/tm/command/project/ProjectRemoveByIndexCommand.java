package ru.tsc.izuev.tm.command.project;

import ru.tsc.izuev.tm.model.Project;
import ru.tsc.izuev.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    private static final String NAME = "project-remove-by-index";

    private static final String DESCRIPTION = "Remove project by index.";

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().findByIndex(index);
        getProjectTaskService().remoteProjectById(project.getId());
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
